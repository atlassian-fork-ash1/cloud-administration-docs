---
title: Index page for Cloud administration API Documentation
platform: platform
product: cloud-administration
category: customerguide
subcategory: index
date: "2018-11-7"
---

# Authorization docs for REST APIs

You need to use API key to manage users and groups. This key gives full administrative access to your organization's directory, allowing the API client to create and update user attributes and change user group membership. You should use the API key as a bearer token in the HTTP request Authorization header.

## How to get API Key
### If user provisioning API is enabled
1.Log in to https://admin.atlassian.com and select User provisioning from the left navigation.

2.Select the API key tab to see the key you created for user provisioning.

### Enable the user provisioning API 
When you enable user provisioning, we create a directory to store user and group information created via the SCIM API. This directory is associated with your organization. Synchronization services periodically pushes updates to user accounts and group memberships from the organization's directory to its sites. Enabling user provisioning also creates an API token, which allows authorized parties to perform operations in this directory.

To enable user provisioning:

1.Log in to https://admin.atlassian.com and select User provisioning from the left navigation.

2.From the User provisioning page, click Create a directory.

![user_prov_empty](/platform/cloud-administration/images/user_prov_empty.png)

3.Enter a Name to identify the user directory, and click Create.

![create_token_api](/platform/cloud-administration/images/create_token_api.png)

4.Copy and save the Directory base URL and the API key to a safe place.

![copy_token_api](/platform/cloud-administration/images/copy_token_api.png)

## How to use API Key
Example:
````bash
# Request
GET /scim/directory/2fb21891-7bee-4c2d-a61a-ade3834c8b2b/Users HTTP/1.1
Host: api.atlassian.com
Content-type: application/json
Authorization: Bearer 0j6lDgrjU7HmGagocgLe

# Response
HTTP/2 200 
content-type: application/scim+json
{
  "schemas": [
    "urn:ietf:params:scim:api:messages:2.0:ListResponse"
  ],
  "totalResults": 0,
  "startIndex": 1,
  "itemsPerPage": 10000,
  "Resources": []
}
````