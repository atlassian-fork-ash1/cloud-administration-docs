---
title: "Latest updates"
platform: platform
product: cloud-administration
category: devguide
subcategory: templates
date: "2018-11-7"
---

[This template is sourced from the Writing Toolkit.](https://developer.atlassian.com/platform/writing-toolkit/latest-updates-page/)

Instructions for using this template:

- Suggested introductions are included for each section; rewrite as desired.
- Remove optional sections that you are not using.
- Ensure you update the YAML metadata (above).
- The filename must be index.md.

# Latest updates

*Start with a brief introduction. A suggested introduction is provided for you.*

We deploy updates to Cloud administration frequently. As a Cloud administration API developer, it's important that you're aware of
the changes. The resources below will help you keep track of what's happening.

## Product roadmap

*This section is optional.*

We maintain a board on Trello to communicate the current high-level priorities for API improvements
in Cloud administration API. It's also the best place to see what has shipped recently. [Check out the board](www.example.com).

## Recent announcements

*List updates hosted on developer.atlassian.com (these should have `updates` in the YAML metadata).*

*Required for ALL products/services, regardless of if it is for internal use only.*

Changes announced in the Atlassian Developer blog are usually described in more detail in this
documentation. The most recent announcements are documented in detail below:

- [Announcement page title](/platform/cloud-administration/example-page.md)
- [Announcement page title](/platform/cloud-administration/example-page.md)

## Atlassian Developer blog

*Provide a link to the Developer blog filtered by tags specific to your product or service.*
(Be sure to change `/categories/jira/` to categories specific to your product.)

*Optional for internal products/services.*

Major changes that affect Cloud administration API developers are also announced in the *Atlassian Developer blog*,
like new modules or the deprecation of API end points. You'll also find handy tips and articles
related to Cloud administration API development.

Check it out and subscribe here: [Atlassian Developer blog](https://developer.atlassian.com/blog/categories/jira/)

## Prior versions

List updates for prior versions, starting with the most recent first. Use a h3 (###) for the
version name/number (e.g., JIRA platform version 7.1)

This section is optional for server products that need a list of prior release notes and updates.

Not required for cloud products.