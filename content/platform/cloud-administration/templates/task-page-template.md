---
title: "Task title"
platform: platform
product: cloud-administration
category: devguide
subcategory: templates
date: "2018-11-7"
---

[This template is sourced from the Writing Toolkit.](https://developer.atlassian.com/platform/writing-toolkit/task-page/)

Instructions for using this template:

 - Remove the HTML comments.
 - Remove optional sections that you are not using.
 - Ensure you update the YAML metadata (above).
 - Update headings, page title, etc.

# Task title

In the introduction, include prerequisites (time, requirements, etc.), and describe the expected result.

If applicable, list prerequisites to completing the task in this section.

## Step title

Each part of the task includes steps. Steps are always explicit and start with an action word.

Task pages never re-explain concepts. Instead, they link to concept pages.

Images are not required after each step, but they should be included when text alone would be
too confusing. Use your best judgment.

## Step title

Chunk multi-part tasks into sections, especially when a context-switch is required.